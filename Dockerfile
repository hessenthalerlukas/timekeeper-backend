FROM openjdk:8-jdk-alpine
VOLUME /tmp
ADD target/timekeeper-1.0.0-SNAPSHOT.jar app.jar
ENV JAVA_OPTS=""
EXPOSE 8080
USER 1001
ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /app.jar
