package com.timekeeper.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.timekeeper.model.Checkpoint;
import com.timekeeper.repositories.CheckpointRepository;

@RestController
public class CheckpointController {

	@Autowired
	CheckpointRepository checkpointRepository;
	
	String userid = "1337";

	@RequestMapping(method = RequestMethod.GET, path = "/checkpoints")
	public ResponseEntity<Iterable<Checkpoint>> getCheckpoints() {

		Iterable<Checkpoint> result = checkpointRepository.findByUserid(userid);
		ResponseEntity<Iterable<Checkpoint>> response = new ResponseEntity<Iterable<Checkpoint>>(result, HttpStatus.OK);
		return response;
	}

	@RequestMapping(method = RequestMethod.POST, path = "/checkpoints")
	public ResponseEntity<Checkpoint> postCheckpoints(@RequestBody Checkpoint checkpoint) {

		Checkpoint savedCheckpoint = checkpointRepository.save(checkpoint);
		ResponseEntity<Checkpoint> response = new ResponseEntity<Checkpoint>(savedCheckpoint, HttpStatus.CREATED);
		return response;
	}

	@RequestMapping(method = RequestMethod.GET, path = "/checkpoints/{id}")
	public ResponseEntity<Checkpoint> getCheckpointById(@PathVariable("id") String id) {

		Optional<Checkpoint> result = checkpointRepository.findById(id);
		ResponseEntity<Checkpoint> response = new ResponseEntity<Checkpoint>(result.get(), HttpStatus.OK);
		return response;
	}
}
