package com.timekeeper.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.timekeeper.model.Checkpoint;

public interface CheckpointRepository extends MongoRepository<Checkpoint, String> {

	Optional<Checkpoint> findById(String id);
	List<Checkpoint> findByUserid(String userid);
}