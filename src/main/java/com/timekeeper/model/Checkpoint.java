package com.timekeeper.model;

import org.springframework.data.annotation.Id;


public class Checkpoint {

	private String id;
	private String datetime; // Unix-Timestamp like 1513193514
	private String latitude; // Breitengrad
	private String longitude; // Längengrad
	private String userid;

	public Checkpoint() {
	}

	public Checkpoint(String datetime, String latitude, String longitude, String userid) {
		super();
		this.datetime = datetime;
		this.latitude = latitude;
		this.longitude = longitude;
		this.userid = userid;
	}

	@Id
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDatetime() {
		return datetime;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

}
